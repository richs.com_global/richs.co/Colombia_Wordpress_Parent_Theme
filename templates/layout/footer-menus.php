<?php 
    $hideFooterNav = get_field('hide_footer_nav');
?>

<div id="footer-menus" role="navigation" aria-label="Footer Navigation">
<?php if(!$hideFooterNav) : ?>
    <?php
        for ($i = 1; $i<=3; $i++) {
            if (has_nav_menu("footer-col-{$i}")){
                wp_nav_menu([
                    'theme_location' => "footer-col-{$i}", 
                    'menu_class' => "styled-menu menu-{$i}", 
                    'container' => false
                ]);
            }
        }
        wp_nav_menu(['theme_location' => "footer-col-4", 'container' => false]);
    ?>
    <div class="button-container">
        <a href="/get-in-touch" class="btn bg-richsred text-white">Contact Us</a>
    </div>
    <div class="social-media">
        <ul>
            <?php if ($facebook = get_field('facebook', 'options')) : ?>
                <li><a href="<?= $facebook; ?>" target="_blank"><img alt="Facebook" src="<?= THEME_URL . '/assets/dist/images/logos/facebook-logo.png'; ?>" /></a></li>
            <?php endif; ?>
            <?php if ($instagram = get_field('instagram', 'options')) : ?>
                <li><a  href="<?= $instagram; ?>" target="_blank"><img alt="Instagram" src="<?= THEME_URL . '/assets/dist/images/logos/instagram-logo.png'; ?>" /></a></li>
            <?php endif; ?>
            <?php if ($linkedin = get_field('linkedin', 'options')) : ?>
                <li><a href="<?= $linkedin; ?>" target="_blank"><img alt="LinkedIn" src="<?= THEME_URL . '/assets/dist/images/logos/linkedin-logo.png'; ?>" /></a></li>
            <?php endif; ?>
            <?php if ($twitter = get_field('twitter', 'options')) : ?>
                <li><a href="<?= $twitter; ?>" target="_blank"><img alt="Twitter" src="<?= THEME_URL . '/assets/dist/images/logos/twitter-logo.png'; ?>" /></a></li>
            <?php endif; ?>
            <?php if ($youtube = get_field('youtube', 'options')) : ?>
                <li><a href="<?= $youtube; ?>" target="_blank"><img alt="YouTube" src="<?= THEME_URL . '/assets/dist/images/logos/youtube-logo.png'; ?>" /></a></li>
            <?php endif; ?>
        </ul>
    </div>
<?php else: 
    if($links = get_field('microsite_footer_nav')) : 
        include('microsite-footer-nav.php'); 
    endif; // end microsite_footer_nav 

endif; 
?>
</div>
