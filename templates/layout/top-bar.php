<?php 
    $hideTopNav = get_field('hide_top_nav');
?>

<?php if(!$hideTopNav) : ?>
    <div id="top-nav-bar">
        <div class="container">
            <span class="global"><i class="fa fa-globe"></i> Rich's Global</span>
            <?php if (has_nav_menu('top-bar')) : ?>
                <nav class="top-nav">
                    <?php wp_nav_menu(['theme_location' => "top-bar"]); ?>
                </nav>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>