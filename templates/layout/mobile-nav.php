<?php 
    $hideTopNav = get_field('hide_top_nav');
    $hideMainNav = get_field('hide_main_nav');
?>

<div id="mobile-nav" style="display:none;">
    <div class="mask"></div>
    <div class="container">
        <?php if ($logo = get_field('mobile_logo', 'options')) : ?>
            <div class="logo">
                <a href="<?= get_home_url(); ?>"><img src="<?= $logo['url']; ?>" alt="Rich Products Mobile Logo"/></a>
            </div>
        <?php endif; ?>
        <div class="toggle">
            <span class="text">Menu</span>
            <span class="burger-icon"></span>
        </div>
        <nav aria-label="Mobile Navigation" class="mobile-nav">
            <div class="toggle">
                <span class="text">Close Menu</span>
                <span class="burger-icon"></span>
            </div>
            <?php if(!$hideMainNav) : ?>
                <?php if (has_nav_menu('mobile')): ?>
                    <?php wp_nav_menu([
                        'theme_location' => "mobile",
                        'link_after' => '<div class="expander"></div>'
                    ]); ?>
                <?php endif; ?>
            <?php 
                else: 
                    if($links = get_field('microsite_nav')) : 
                        include('microsite-nav.php'); 
                    endif; // end microsite_nav 

                endif; // end if(!$hideMainNav) 
            ?>
        </nav>
    </div>
</div>