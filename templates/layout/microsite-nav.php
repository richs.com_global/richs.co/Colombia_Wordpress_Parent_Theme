<?php if($links = get_field('microsite_nav')) : ?>

        <div class="menu-primary-nav-container">
            <ul id="menu-primary-nav" class="menu">
            <?php
                foreach($links as $link) { ?>
                    <li class="menu-item"><a href="<?= $link['link']['url']; ?>" target="<?= $link['link']['target']; ?>" ><?= $link['link']['title'];?></a></li>
                <?php } ?>
            </ul>
        </div>

<?php endif; // end microsite_nav ?>