<?php 
    $hideTopNav = get_field('hide_top_nav');
    $hideMainNav = get_field('hide_main_nav');
?>

<main-nav id="main-nav" class="<?= $hideTopNav ? 'top-nav-hidden':'';?>">
    <div class="container">
        <?php if ($logo = get_field('logo', 'options')) : ?>
            <div class="logo">
                <a href="<?= get_home_url(); ?>"><img alt="Rich's Logo" src="<?php echo $logo['url']; ?>" /></a>
            </div>
        <?php endif; ?>

        <?php if(!$hideMainNav) :?>
            <?php if (has_nav_menu('primary')): ?>
                <nav aria-label="Primary Navigation" class="primary-nav">
                    <?php wp_nav_menu(['theme_location' => "primary"]); ?>
                </nav>
            <?php endif; ?>
        <?php else: 

            if($links = get_field('microsite_nav')) : ?>
                <nav aria-label="Primary Navigation" class="primary-nav">
                    <?php include('microsite-nav.php'); ?>
                </nav>
            <?php endif; // end microsite_nav ?>
            
        <?php endif; //end if(!$hideMainNav) ?>

    </div>
</main-nav>