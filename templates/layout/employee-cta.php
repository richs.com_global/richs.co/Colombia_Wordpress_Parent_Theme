<?php if ($cta != null) : ?>
    <div class="employee-cta">
        <a href="<?= $cta['url']; ?>" target="<?= $cta['target']; ?>"><?= $cta['title']; ?></a>
    </div>
<?php endif; ?>