<?php if($block): ?>
    <div class="container">
        <h1 class="block-title"><?= $block['title']; ?></h1>
        <div class="flex flex-row content-wrapper">
            <?php foreach($block['links'] as $link) : ?>
                <div class="link-card">
                    <div class="wrapper">
                        <div class="card-content">
                            <h3 class="link-title"><?= $link['link_title']; ?></h3>
                            <div class="link-text"><?= $link['link_text']; ?></div>
                        </div>
                        <i class="fas fa-chevron-right"></i>
                    </div>
                    <a class="full" href="<?= $link['link']['url']; ?>" target="<?= $link['link']['target']; ?>"><?= $link['link_title']; ?></a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>