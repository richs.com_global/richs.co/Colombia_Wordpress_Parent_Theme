<?php if ($block) :
    $options = [
        'slidesToShow' => 1,
        'slidesToScroll' => 1,
        'prevArrow' => '',
        'nextArrow' => '<button type="button" class="slick-next"></button>',
        'speed' => 800,
        'dots' => false,
    ];

    $slide_count = count($block['slides']);
    $show_timer = $slide_count > 1;
    $bannerHeight = $block['adjust_height'] ? 'min-height:'.$block['banner_height'].';':'';
    ?>
    <div class="banner-carousel-container">
        <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel timed-carousel">
            <?php for ($i = 0; $i < count($block['slides']); $i++) : $slide = $block['slides'][$i]; ?>
                <div class="background-wrapper" style="background-image:url(<?= $slide['background_image']['url']; ?>); <?= $bannerHeight; ?>">
                    <?php
                    $overlay_opacity = 0;
                    if ($slide['add_background_overlay'] === true) {
                        $overlay_opacity = $slide['background_overlay_opacity'];
                    }
                    ?>
                    <div class="overlay gradient <?= $slide['increase_text_contrast'] ? "increased-contrast" : "" ; ?>" style="opacity: <?= $overlay_opacity; ?>;"></div>
                    <div class="container">
                        <div class="content side-<?= $slide['text_side'] ?>">
                            <?php if(!empty($slide['title'])) : ?>
                            <h2 class="title"><?= $slide['title']; ?></h2>
                            <?php endif; //title ?>

                            <?php if(!empty($slide['text'])) : ?>
                            <p class="text"><?= $slide['text']; ?></p>
                            <?php endif; //text ?>

                            <?php if(!empty($slide['country_code']) || !empty($slide['name']) || !empty($slide['occupation'])) : ?>
                            <div class="person-info">
                                <?php if($slide['country_code'] != "") : ?>
                                    <span class="mr-1 flag-icon flag-icon-<?= $slide['country_code']; ?>"></span>
                                <?php endif; ?>
                                <span class="name"><?= $slide['name']; ?></span>
                                <span class="occupation"><?= $slide['occupation']; ?></span>
                            </div>
                            <?php endif; //person-info ?>

                            <?php if ($button = $slide['button']) : ?>
                                <a <?= !empty($slide['recipe_image']) ? 'data-remodal-target="recipe-'.$i.'"' : "";?> class="btn" href="<?= !empty($slide['recipe_image']) ? "#" : $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                            <?php endif; ?>
                            <?php if ($show_timer) : ?>
                                <div class="progress-timer"></div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if($slide['recipe_image']): ?>
                        <div class="remodal photo" data-remodal-options="hashTracking: false" data-remodal-id="recipe-<?=$i; ?>">
                            <button data-remodal-action="close" class="remodal-close"></button>
                            <div class="remodal-close actions">
                                <a title="Download" class="download" href="<?=$slide['recipe_image']['url']?>" download>
                                    <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M12 10v6m0 0l-3-3m3 3l3-3m2 8H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path></svg>
                                </a>
                                <a title="E-Mail" class="email" href="mailto:?subject=<?= urlencode('I found this recipe you might like'); ?>&body=<?= urlencode($slide['recipe_image']['url']); ?>">
                                    <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" class="w-8 h-8"><path d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path></svg>
                                </a>
                            </div>
                            <img class="mx-auto h-auto" src="<?=$slide['recipe_image']['url']?>">
                        </div>
                    <?php endif; ?>
                </div>
            <?php endfor; ?>
        </slick-carousel>
    </div>
<?php endif; ?>