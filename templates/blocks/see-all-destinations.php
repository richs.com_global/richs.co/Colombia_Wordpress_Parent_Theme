<?php

use Cn\Theme;

$unorderedPosts = Theme::get_all_regions(true);
$posts = array();

foreach($unorderedPosts as $post){
   $posts[get_field('group_label', $post)][] = $post;
}
?>
<div class="bg-cover bg-center" style="background-image:url(<?= $block['background_image']['url']; ?>);">
    <div class="<?= $block['style']; ?> container">
        <h2 class="header"><?= $block['title'];?></h2>
        <destinations>
            <content-box title="<?= $block['button_one']['title']; ?>">
                <?php include 'all-destinations/pop-up.php'; ?>
            </content-box>
            <content-box title="<?= $block['button_two']['title']; ?>">
                <?php gravity_form( $block['gravity_form_id'] , false, false, false, '', true ); ?>
            </content-box>
        </destinations>
    </div>
</div>