<?php if($block): ?>
    <div class="container">
        <div class="flex flex-row content-wrapper image-position-<?= $block['image_position']; ?> image-size-<?= $block['image_size'] ?? "default"; ?>">
            <div class="image-container">
                <img src="<?= $block['image']['url']; ?>" alt="<?= $block['image']['alt']; ?>" />
            </div>

            <div class="text-container">
                <?php if (!empty($block['title'])) : ?>
                    <h2 class="block-title"><?= $block['title']; ?></h2>
                <?php endif; ?>
                <div class="text"><?= $block['text']; ?></div>
                <?php if ($button = $block['button']) : ?>
                    <a class="btn btn-red icon-btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?><i class="fas fa-chevron-right"></i></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>