<?php $button = $block['button']; ?>
<div class="background" style="background-image: url(<?= $block['background_image']['url']; ?>);">
    <div class="container">
        <div class="cta-box">
            <div class="title-container">
                <h2 class="title"><?= $block['title']; ?></h2>
                <img src="<?= $block['category_icon']['url']; ?>" >
            </div>
            <div class="text"><?= $block['text']; ?></div>
            <?php if ($button) : ?>
                <a class="btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
            <?php endif; ?>
        </div>
    </div>
</div>