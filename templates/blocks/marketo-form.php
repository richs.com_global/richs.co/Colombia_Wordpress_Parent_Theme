<div class="container">
    <div class="form-container <?= $block['style']; ?>">
        <div class="cn-col cn-col-1">
            <h2 class="block-title"><?= $block['title']; ?></h2>
            <p><?= $block['text']; ?></p>
        </div>
        <div class="cn-col cn-col-2">
            <?php 

            //default the form ID to the customer care form
            if(!isset($block['marketo_id'])) {$block['marketo_id'] = "1044";}
            $props = 'marketoid="'.$block['marketo_id'].'" ';

            //marketo theme settings
            if ($marketoUrl = get_field('marketo_url', 'options')){$props .= 'marketourl="'.$marketoUrl.'" ';}
            if ($marketoInstanceId = get_field('marketo_instanceId', 'options')){$props .= 'marketoinstanceid="'.$marketoInstanceId.'" ';}
            if ($marketoThankYouTitle = get_field('marketo_thankyou_title', 'options')){$props .= 'marketothankyoutitle="'.$marketoThankYouTitle.'" ';}
            if ($marketoThankYouMessage = get_field('marketo_thankyou_message', 'options')){$props .= 'marketothankyoumessage="'.$marketoThankYouMessage.'" ';}

            ?>

            <marketo-form <?= $props; ?>><form id="mktoForm_<?= $block['marketo_id']; ?>"></form></marketo-form>
        </div>
    </div>
</div>