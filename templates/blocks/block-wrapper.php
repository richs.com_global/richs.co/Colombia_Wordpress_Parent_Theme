<?php use Cn\Blocks\Block; ?>
<section class="block <?= Block::get_default_html_classes($block); ?>" id="<?= $block['html_attributes']['id'];?>">
    <?php include($template); ?>
</section>