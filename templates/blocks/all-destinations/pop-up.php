<?php 
$global = null;
$counter = 0; 
foreach($posts as $key => $value): 
    if(strtolower($key) == "global"){
        $global = $value;
        continue;
    }
?>
    <div class="col"> 
        <div class="link-list"> 
            <h6 class='text-with-border red'><?= $key ?></h6>
                <ul>
                    <?php
                        foreach($value as $post):
                            $marketingSites = get_field('marketing_sites', $post);
                                foreach($marketingSites as $site):
                                    if(get_field('coming_soon', $post)){
                                        echo '<li><span>'.$site['link']['title'].'<small>Coming Soon</small></span></li>';
                                    } else { 
                                    ?> 
                                        <li><a class="text-black" target="<?= $site['link']['target'];?>"  href="<?= $site['link']['url']?>"><?= $site['link']['title']?></a></li>
                                    <?php 
                                    }
                                endforeach;
                        endforeach;
                    ?>
                </ul>
            </div>
            <?php if($counter == count($posts) - 2 && $global): ?>
                <a href="<?= get_home_url() . '/our-products'; ?>" class="text-xs btn btn-red btn-icon flex flex-row justify-between items-center mt-4">Global Experience<i class="fas fa-chevron-right"></i></a>
            <?php endif; ?>
        </div>
    <?php
    $counter++;
    endforeach; 
?>