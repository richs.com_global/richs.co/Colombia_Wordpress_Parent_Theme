<div class="container">
    <div class="<?= $block['style']; ?>">
        <div class="header">
            <h2 class="section-title"><?= $block['title']; ?></h2>
            <span class="scroll-to-explore inline-block">switch tabs to address your need </span>
        </div>
        <tabs>
            <?php $selected = "true"; foreach($block['tabs'] as $tab): ?>
                <tab name="<?= $tab['title'] ?>" :selected="<?= $selected; ?>">
                    <?php gravity_form( $tab['gravity_form_id'] , false, false, false, '', true ); ?>
                </tab>
            <?php $selected = "false"; endforeach; ?>
        </tabs>
    </div>
</div>