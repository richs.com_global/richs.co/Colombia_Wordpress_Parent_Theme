<div class="container">
    <?php if($block['title'] != "") : ?>
        <h2 class="block-title"><?= $block['title']; ?></h2>
    <?php endif; ?>
    <div class="categories">
        <?php foreach($block['categories'] as $category) : ?>
            <div class="card-wrapper">
                <div class="category card size-medium">
                    <?php if ($link = $category['link']) : ?>
                        <a class="full-link" href="<?= $link['url']; ?>" target="<?= $link['target']; ?>" style="position:absolute; top:0; bottom: 0; left:0; right:0; z-index:3; opacity: 0;"><?= $category['title']; ?></a>
                    <?php endif; ?>
                    <div class="background" style="background-image:url(<?= $category['background_image']['url']; ?>);"></div>
                    <?php
                        $overlay_opacity = 0;
                        if ($category['add_background_overlay'] === true) {
                            $overlay_opacity = $category['background_overlay_opacity'];
                        }
                    ?>
                    <div class="overlay" style="opacity: <?= $overlay_opacity; ?>;"></div>
                    <?php if ($category['hide_header'] !== true && $country_code = $block['country_code']) : ?>
                        <div class="header">
                            <span class="mr-1 flag-icon flag-icon-<?= $country_code; ?>"></span>
                            <span class="text">Rich's <?= country_name_from_abbreviation($country_code); ?></span>
                        </div>
                    <?php endif; ?>
                    <div class="card-content">
                        <h2 class="title"><?= $category['title']; ?></h2>
                        <div class="text"><?= $category['text']; ?></div>

                        <?php if ($button = $category['card_button_1']) : ?>
                            <div>
                                <a class="btn icon-btn button-1" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?><i class="fas fa-external-link-square-alt"></i></a>
                            </div>
                        <?php endif; ?>
                        <?php if ($button = $category['card_button_2']) : ?>
                            <div>
                                <a class="btn icon-btn button-2" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?><i class="fas fa-external-link-square-alt"></i></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>