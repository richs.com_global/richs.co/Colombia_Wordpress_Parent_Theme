<div class="container">
    <h2 class="block-title"><?= $block['title']; ?></h2>
    <div class="flex">
        <?php foreach ($block['people'] as $person) : $personID = $person->ID; ?>
            <a class="person card" href="<?= get_permalink($personID); ?>" style="background-image:url(<?= get_field('headshot', $personID)['url']; ?>);">
                <div class="text-container relative">
                    <h2 class="name"><?= get_field('first_name', $personID); ?><br><?= get_field('last_name', $personID); ?></h2>
                    <div class="job-title"><?= get_field('job_title', $personID); ?></div>
                </div>
                <div class="absolute bg-black overlay"></div>
            </a>        
        <?php endforeach; ?>
    </div>
</div>