<div class="container">
    <?php if ($title = $block['title']) : ?>
        <h2 class="block-title"><?= $title; ?></h2>
    <?php endif; ?>
    <div class="video-container">
        <div class="placeholder-image" style="background-image:url(<?= $block['placeholder_image']['url']; ?>);" data-remodal-target="video-<?= $block['youtube_id']; ?>">
            <?php
            $overlay_opacity = 0;
            if ($block['add_background_overlay'] === true) {
                $overlay_opacity = $block['background_overlay_opacity'];
            }
            ?>
            <div class="overlay gradient" style="opacity: <?= $overlay_opacity; ?>;"></div>
            <div class="play-icon"></div>
        </div>
    </div>
</div>

<section class="remodal video" data-remodal-options="hashTracking: false" data-remodal-id="video-<?= $block['youtube_id']; ?>">
    <button data-remodal-action="close" class="remodal-close"></button>
    <iframe class="video"  width="560" height="315" data-video="https://www.youtube.com/embed/<?= $block['youtube_id']; ?>?enablejsapi=1&rel=0&modestbranding=1" frameborder="0" allowfullscreen></iframe>
</section>