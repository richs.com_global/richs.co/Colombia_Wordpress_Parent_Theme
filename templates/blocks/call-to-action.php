<?php $button = $block['button']; ?>
<div class="container">
    <div class="cta-box">
        <h2 class="title"><?= $block['title']; ?></h2>
        <div class="text"><?= $block['text']; ?></div>
        <a class="btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
    </div>
</div>