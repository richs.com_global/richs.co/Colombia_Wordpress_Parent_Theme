<div class="container">
    <div class="flex">
        <?php foreach ($block['pillars'] as $pillar) : ?>
            <div class="pillar">
                <?php if ($image = $pillar['image']) : ?>
                    <div class="image-container">
                        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" />
                    </div>
                        <h3 class="title"><?= $pillar['title']; ?></h3>
                        <div class="copy"><?= $pillar['content']; ?></div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>