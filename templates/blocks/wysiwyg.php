<div class="container">
    <?php if($block['title'] != '') { ?>
        <h2 class="block-title"><?= $block['title']; ?></h2>
    <?php } ?>
    <div class="content"><?= $block['content']; ?></div>
</div>