<?php if($block):
    $brands = $block['brands'];

    $options = [
        'slidesToShow' => 5,
        'slidesToScroll' => 1,
        'prevArrow' => '',
        'nextArrow' => '<button type="button" class="slick-next"></button>',
        'speed' => 800,
        'dots' => true,
        'touchThreshold' => 100,
        'infinite' => false,
        'variableWidth' => true,
        'responsive' => [
            [
                'breakpoint' => 1024,
                'settings' => [
                    'slidesToShow' => 4,
                    'arrows' => false,
                    'autoplay' => true,
                    'autoplaySpeed' => 3000,
                ]
            ],
            [
                'breakpoint' => 768,
                'settings' => [
                    'slidesToShow' => 2,
                    'arrows' => false,
                    'autoplay' => true,
                    'autoplaySpeed' => 3000,
                ]
            ],
            [
                'breakpoint' => 567,
                'settings' => [
                    'slidesToShow' => 2,
                    'arrows' => false,
                    'autoplay' => true,
                    'autoplaySpeed' => 3000,
                    'adaptiveHeight' => true
                ]
            ]
        ]
    ];
?>
    <div class="brand-carousel">
        <div class="container">
            <?php if ($title = $block['title']) : ?>
                <h2 class="block-title"><?= $title; ?></h2>
            <?php endif; ?>
            <div class="carousel-container">
                <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel">
                    <?php foreach ($block['brands'] as $brand) : $hoverImage = $brand['hover_image']; ?>
                        <div class="brand <?php echo $hoverImage != null ? "has-hover-image" : ""; ?>">
                            <div class="flex">
                                <div class="image-container">
                                    <img class="logo" src="<?= $brand['logo']['url']; ?>" >
                                    <?php if ($hoverImage) : ?>
                                        <img class="hover-image" src="<?= $hoverImage['url']; ?>" />
                                    <?php endif; ?>
                                </div>
                                <div class="brand-content">
                                    <div class="copy h-full flex-grow">
                                        <?= $brand['copy']; ?>
                                    </div>
                                    <?php if ($button = $brand['button']) : ?>
                                        <a class="btn bg-richsred text-white text-left" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?> <i class="fas fa-external-link-square-alt ml-1"></i></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </slick-carousel>
            </div>
        </div>
    </div>
<?php endif; ?>