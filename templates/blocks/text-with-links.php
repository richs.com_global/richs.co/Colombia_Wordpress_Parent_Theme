<div class="container">
    <div class="flex">
        <div class="content-container">
            <h2 class="block-title"><?= $block['title']; ?></h2>
            <div class="text"><?= $block['text']; ?></div>
        </div>
        <div class="links">
            <ul>
                <?php foreach($block['links'] as $item) : $link = $item['link']; ?>
                    <li>
                        <a href="<?= $link['url']; ?>" target="<?= $link['target']; ?>"><?= $link['title']; ?><i class="fas fa-chevron-right"></i></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>