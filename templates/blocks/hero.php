<?php if ($block) : ?>

    <?php
    $background_url = THEME_URL . '/assets/dist/images/richs-hero-image.jpg';
    if ($background = $block['background_image']) {
        $background_url = $background['url'];
    }

    $hero_style = $block['hero_style'];
    if (!$hero_style) {
        $hero_style = "default";
    }
    ?>
    <div class="background hero-style-<?= $hero_style; ?>" style="background-image:url(<?= $background_url; ?>);">
        <?php
        $overlay_opacity = 0;
        if ($block['add_background_overlay'] === true) {
            $overlay_opacity = $block['background_overlay_opacity'];
        }
        ?>
        <div class="overlay gradient" style="opacity: <?= $overlay_opacity; ?>;"></div>
        <div class="container">
            <div class="flex">
                <div class="content">
                    <?php if (!empty($block['top_title'])) : ?>
                        <h2 class="top-title"><?= $block['top_title']; ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($block['title'])) : ?>
                        <h1 class="title"><?= $block['title']; ?></h1>
                    <?php endif; ?>
                    <?php if ($hero_style == "default" && !empty($block['copy'])) : ?>
                        <div class="copy"><?= $block['copy']; ?></div>
                    <?php endif; ?>
                    <?php if ($hero_style == "quote" && !empty($block['attribution'])) : ?>
                        <div class="attribution"><?= $block['attribution']; ?></div>
                    <?php endif; ?>
                </div>
                <?php /* removing for now
                <div class="secondary">
                    <button class="scroll-to-explore">Scroll To Explore</button>
                    <?php if (is_front_page()) : ?>
                        <form role="search" method="get" id="top-search" class="searchform" action="<?= get_home_url(); ?>">
                            <input placeholder="Search for anything" type="text" name="s" value="" class="mb-0" />
                            <button type="submit" class="btn">Search <i class="fas fa-chevron-right"></i></button>
                        </form>
                    <?php endif; ?>
                </div>
                */ ?>
            </div>
        </div>
    </div>
    <?php if ($block['is_add_announcement']) : ?>
        <div class="announcement">
            <div class="container">
                <div class="flex">
                    <div class="text"><?= $block['announcement']['text']; ?></div>
                    <?php if ($button = $block['announcement']['button']) : ?>
                        <div class="button-container">
                            <a class="btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>