<?php if($block):
    $options = [
        'slidesToShow' => 1,
        'slidesToScroll' => 1,
        'prevArrow' => '',
        'nextArrow' => '<button type="button" class="slick-next"></button>',
        'speed' => 800,
        'dots' => true,
    ];

    $slide_count = count($block['slides']);
    $show_timer = $slide_count > 1;
?>
    <div class="container">
        <?php if($block['title'] != '') : ?>
        <h1 class="block-title mb-0"><?= $block['title']; ?></h1>
        <?php endif; ?>
        <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel timed-carousel">
            <?php foreach($block['slides'] as $slide) : ?>
                <div class="item">
                    <div class="flex video-position-<?= $slide['video_position']; ?>">
                        <div class="video-container">
                            <?php if ($show_timer) : ?>
                                <div class="progress-timer red"></div>
                            <?php endif; ?>
                            <?php if($slide['use_image'] && $slide['image']): ?>
                                <img src="<?= $slide['image']['url'] ?>" alt="<?= $slide['image']['title']; ?>"/>
                            <?php else: ?>
                                <video loop muted playsinline>
                                    <source src="<?php echo $slide['video']['url']; ?>" type="video/mp4">
                                </video>
                            <?php endif; ?>
                        </div>
                        <div class="text-container">
                            <h1 class="block-title"><?= $slide['title']; ?></h1>
                            <div class="text"><?= $slide['text']; ?></div>
                            <?php if ($button = $slide['button']) : ?>
                                <a class="btn btn-red icon-btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?><i class="fas fa-chevron-right"></i></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </slick-carousel>
    </div>
<?php endif; ?>