<?php

namespace Cn;

class AjaxHandler {
    use Singleton;

    public function __construct()
    {
        $this->register_routes();
    }

    public function register_routes() {
        // Regional link AJAX
        add_action( 'wp_ajax_get_regional_link', [$this, 'ajax_get_regional_link']);
        add_action( 'wp_ajax_nopriv_get_regional_link', [$this, 'ajax_get_regional_link']);

        // Visitor Params
        add_action( 'wp_ajax_get_visitor_params', [$this, 'ajax_get_visitor_params']);
        add_action( 'wp_ajax_nopriv_get_visitor_params', [$this, 'ajax_get_visitor_params']);
    }

    public function ajax_get_visitor_params() {
        $postParams = filter_input_array(INPUT_POST);
        $countryCode = $postParams['countryCode'];
        $countryName = get_country_codes_array()[strtoupper($countryCode)];

        $response = [
            'countryCode' => $countryCode,
            'countryName' => $countryName
        ];

        // Check if this visitor is coming from the WHQ and if so, get the employee cta
        if ($postParams['workLocation'] == "WHQ") {
            $response['employeeCta'] = $this->get_employee_cta();
        }

        $region = Theme::get_region($countryCode);
        if ($region) {
            $response['region'] = $region->post_title;
            if (!get_field('coming_soon', $region->ID)) {
                $response['marketingSites'] = get_field('marketing_sites', $region->ID);
            }
        }

        wp_send_json($response);
        wp_die();
    }

    public function ajax_get_regional_link() {
        $countryCode = filter_input(INPUT_POST, 'countryCode');
        $globalPageId = filter_input(INPUT_POST, 'globalID');

        $pageUrl = null;

        if ($countryCode != null && $globalPageId != null) {
            $pageUrl = Theme::get_region_page_url($globalPageId, $countryCode);
        }

        wp_send_json([
            'url' => $pageUrl
        ]);
        wp_die();
    }

    private function get_employee_cta() {
        if (!get_field('enable_employee_cta', 'options')) return null;
        ob_start();
        
        /** This variable is used in the template. Do not remove. */
        $cta = get_field('employee_cta_button', 'options');

        $cta_template = locate_template("templates/layout/employee-cta.php");
        include($cta_template);
        return ob_get_clean();
    }
}