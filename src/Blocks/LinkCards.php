<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class LinkCards extends Block
{
    public function __construct()
    {
        parent::register_block(
            'link-cards',
            [
                'title'           => 'Link Cards',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['link', 'card']
            ]
        );
    }
}