<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class SimpleModal extends Block
{
    public function __construct()
    {
        parent::register_block(
            'simple-modal',
            [
                'title'           => 'Simple Pop Up',
                'category'		  => 'layout',
                'icon'			  => 'slides',
                'keywords'		  => ['modal','pop','up']
            ]
        );
    }
}