<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class TextWithLinks extends Block
{
    public function __construct()
    {
        parent::register_block(
            'text-with-links',
            [
                'title'           => 'Text With Links',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'links']
            ]
        );
    }
}