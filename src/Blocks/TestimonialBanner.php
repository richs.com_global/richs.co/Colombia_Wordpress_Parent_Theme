<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class TestimonialBanner extends Block
{
    public function __construct()
    {
        parent::register_block(
            'testimonial-banner',
            [
                'title'           => 'Testimonial Banner',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['testimonial', 'quote', 'banner']
            ]
        );
    }
}