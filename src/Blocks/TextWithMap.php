<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class TextWithMap extends Block
{
    public function __construct()
    {
        parent::register_block(
            'text-with-map',
            [
                'title'           => 'Text With Map',
                'category'		  => 'layout',
                'icon'			  => 'admin-site',
                'keywords'		  => ['text', 'map', 'text with map']
            ]
        );
    }
}