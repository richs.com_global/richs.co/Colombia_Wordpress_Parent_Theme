<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class Banner extends Block
{
    public function __construct()
    {
        parent::register_block(
            'banner',
            [
                'title'           => 'Banner',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['banner']
            ]
        );
    }
}