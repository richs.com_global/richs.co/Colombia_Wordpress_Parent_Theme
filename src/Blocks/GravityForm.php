<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class GravityForm extends Block
{
    public function __construct()
    {
        parent::register_block(
            'gravity-form',
            [
                'title'           => 'Gravity Form',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['submission']
            ]
        );
    }
}