<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class TabbedForm extends Block
{
    public function __construct()
    {
        parent::register_block(
            'tabbed-form',
            [
                'title'           => 'Tabbed Form',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['form', 'tabs', 'tabbed']
            ]
        );
    }
}