<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class Map extends Block
{
    public function __construct()
    {
        parent::register_block(
            'map',
            [
                'title'           => 'Map',
                'category'		  => 'layout',
                'icon'			  => 'admin-site',
                'keywords'		  => ['map']
            ]
        );
    }
}