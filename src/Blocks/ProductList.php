<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class ProductList extends Block
{
    public function __construct()
    {
        parent::register_block(
            'product-list',
            [
                'title'           => 'Product List',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['products']
            ]
        );
    }
}