<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class MarketoForm extends Block
{
    public function __construct()
    {
        parent::register_block(
            'marketo-form',
            [
                'title'           => 'Marketo Form',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['form', 'marketo']
            ]
        );
    }
}