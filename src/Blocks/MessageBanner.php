<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class MessageBanner extends Block
{
    public function __construct()
    {
        parent::register_block(
            'message-banner',
            [
                'title'           => 'Message Banner',
                'category'		  => 'layout',
                'icon'			  => 'money',
                'keywords'		  => ['message', 'quote', 'banner']
            ]
        );
    }
}