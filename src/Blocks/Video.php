<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class Video extends Block
{
    public function __construct()
    {
        parent::register_block(
            'video',
            [
                'title'           => 'Video',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['video', 'youtube', 'media']
            ]
        );
    }
}