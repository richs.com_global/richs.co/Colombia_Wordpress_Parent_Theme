<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class Timeline extends Block
{
    public function __construct()
    {
        parent::register_block(
            'timeline',
            [
                'title'           => 'Timeline',
                'category'		  => 'layout',
                'icon'			  => 'format-video',
                'keywords'		  => ['timeline']
            ]
        );
    }
}