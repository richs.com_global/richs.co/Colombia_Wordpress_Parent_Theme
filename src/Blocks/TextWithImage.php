<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class TextWithImage extends Block
{
    public function __construct()
    {
        parent::register_block(
            'text-with-image',
            [
                'title'           => 'Text With Image',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'image', 'text with image']
            ]
        );
    }
}