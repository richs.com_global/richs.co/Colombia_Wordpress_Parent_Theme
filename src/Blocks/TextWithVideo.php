<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class TextWithVideo extends Block
{
    public function __construct()
    {
        parent::register_block(
            'text-with-video',
            [
                'title'           => 'Text With Video',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['video', 'movie', 'mp4']
            ]
        );
    }
}