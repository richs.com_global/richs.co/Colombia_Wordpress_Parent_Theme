<?php

namespace Cn\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */
class ImageGrid extends Block
{
    public function __construct()
    {
        parent::register_block(
            'image-grid',
            [
                'title'           => 'Product Image Grid',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['gallery', 'images']
            ]
        );
    }
}