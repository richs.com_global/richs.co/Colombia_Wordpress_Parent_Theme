<?php

namespace Cn;

class Theme
{
    use Singleton;

    public function __construct()
    {
        $this->unregister_actions();

        $this->register_actions();

        $this->register_filters();
    }

    /**
     * Unregister actions
     */
    protected function unregister_actions()
    {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_action('wp_head', 'wp_generator', 10, 0);
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'rsd_link');
    }

    /**
     * Register actions
     */
    protected function register_actions()
    {
        add_action('after_switch_theme', [$this, 'theme_activation']);

        add_action('after_setup_theme', [$this, 'enable_theme_support']);
        add_action('after_setup_theme', [$this, 'add_image_sizes']);

        add_action('init', [$this, 'force_location']);
        add_action('init', [$this, 'register_routes']);
        add_action('init', [$this, 'register_menus']);
        add_action('init', [$this, 'add_shortcodes']);

        add_action('widgets_init', [$this, 'unregister_widgets']);
        add_action('widgets_init', [$this, 'register_widgets']);
        add_action('widgets_init', [$this, 'register_widget_areas']);

        add_action('wp_enqueue_scripts', [$this, 'deregister_scripts'], 9);
        add_action('wp_enqueue_scripts', [$this, 'register_styles'], 10);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_styles'], 11);
        add_action('wp_enqueue_scripts', [$this, 'register_scripts'], 10);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts'], 11);

        add_action('admin_enqueue_scripts', [$this, 'enqueue_admin_styles'], 10);

        add_action( 'pre_get_posts', [$this, 'update_posts_per_page'] );

        // Dyanmic CRM Actions
        add_filter('gform_dynamicscrm_case', [$this, 'add_elogic_custom_data'], 10, 5);

        // Hreflang sitemap items
        add_filter( 'wpseo_sitemap_index', [$this, 'add_hreflang_sitemap']);
    }

    protected function register_filters()
    {
        add_filter('post_thumbnail_html', [$this, 'remove_thumbnail_dimensions'], 10, 3);

        add_filter('custom_menu_order', [$this, 'reorder_admin_menu']);
        add_filter('menu_order', [$this, 'reorder_admin_menu']);

        add_filter('excerpt_more', [$this, 'replace_excerpt_more']);
        add_filter('image_size_names_choose', [$this, 'image_size_options']);
        // add_filter('max_srcset_image_width', function() { return 1; });

        // SVG Uploads
        add_filter('upload_mimes', [$this, 'cc_mime_types']);

        add_filter('get_search_form', [$this, 'get_search_form'], 100, 1);

        // Yoast SEO
        add_filter('wpseo_metabox_prio', function() { return 'low'; });

        add_filter('allowed_block_types', [$this, 'allowed_block_types'], 10, 2);

        // Block Templates
        add_filter( 'register_post_type_args', [$this, 'register_block_templates'], 20, 2 );

        //gravity forms
        // GF method: http://www.gravityhelp.com/documentation/gravity-forms/extending-gravity-forms/hooks/filters/gform_init_scripts_footer/
        add_filter( 'gform_ajax_spinner_url', [$this, 'spinner_url'], 10, 2 );

        
        add_filter( 'gform_field_value_location', [$this, 'set_gravityforms_country']);
        add_filter( 'gform_field_value_region', [$this, 'set_gravityforms_region']);

        //https://gist.github.com/eriteric/5d6ca5969a662339c4b3
        add_filter( 'gform_init_scripts_footer', '__return_true' );
        add_filter( 'gform_cdata_open', [$this, 'wrap_gform_cdata_open'], 1 );
        add_filter( 'gform_cdata_close', [$this, 'wrap_gform_cdata_close'], 99 );
    }

    /**
     * Flush your rewrite rules and other theme activation procedures
     */
    public function theme_activation()
    {
        // Check and Set the reserved built-in image sizes
        if(get_option('thumbnail_size_w')    != 240)  update_option('thumbnail_size_w',    240);
        if(get_option('thumbnail_size_h')    != 240)  update_option('thumbnail_size_h',    240);
        if(get_option('thumbnail_crop')      != 1)    update_option('thumbnail_crop',      1);
        if(get_option('medium_size_w')       != 640)  update_option('medium_size_w',       640);
        if(get_option('medium_size_h')       != 640)  update_option('medium_size_h',       640);
        if(get_option('medium_large_size_w') != 768)  update_option('medium_large_size_w', 768); // leave height unbounded
        if(get_option('large_size_w')        != 1024) update_option('large_size_w',        1024);
        if(get_option('large_size_h')        != 1024) update_option('large_size_h',        1024);

        flush_rewrite_rules();
    }

    /**
     * Enable theme support features
     */
    public function enable_theme_support()
    {
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
    }

    /**
     * Adds gallery image sizes
     */
    public function add_image_sizes()
    {
        add_image_size('small', 480, 480);
        add_image_size('card-square', 570, 570, true);
        add_image_size('x_large', 1200, 1200);
        add_image_size('2x_large', 1440, 1440);
    }

    /**
     * Make custom image sizes selectable from media library
     */
    public function image_size_options( $sizes )
    {
        return [
            'thumbnail'    => __( 'Thumbnail' ),
            'small'        => __( 'Small' ),
            'card-square'  => __('CardSquare'),
            'x_large'      => __( 'xLarge' ),
            '2x_large'     => __( '2xLarge' ),
            'full'         => __( 'Full' )
        ];
    }

    /**
     * Register custom routes
     */
    public function register_routes()
    {
        //
    }

    /**
     * Register menus
     */
    public function register_menus()
    {
        register_nav_menus([
            'primary' => __('Primary Navigation'),
            'footer' => __('Footer Navigation'),
            'top-bar' => __('Top Bar'),
            'mobile' => __('Mobile'),
            'footer-col-1' => __('Footer - Column 1'),
            'footer-col-2' => __('Footer - Column 2'),
            'footer-col-3' => __('Footer - Column 3'),
            'footer-col-4' => __('Footer - Column 4'),
        ]);
    }

    /**
     * Register shortcodes
     */
    public function add_shortcodes()
    {
        //
    }

    /**
     * Unregister widgets that we are not going to support
     */
    public function unregister_widgets()
    {
        unregister_widget('WP_Widget_Calendar');
        unregister_widget('WP_Widget_Links');
        unregister_widget('WP_Widget_Meta');
        unregister_widget('WP_Widget_Search');
        unregister_widget('WP_Widget_Recent_Comments');
        unregister_widget('WP_Widget_RSS');
        unregister_widget('WP_Widget_Tag_Cloud');
        unregister_widget('WP_Widget_Media_Audio');
        unregister_widget('WP_Widget_Media_Video');
        unregister_widget('WP_Widget_Media_Gallery');
    }


    /**
     * Register all widgets
     */
    public function register_widgets()
    {}

    /**
     * Register all widget areas
     *
     * @hooked widgets_init - 10
     */
    public function register_widget_areas()
    {
        register_sidebar(array(
            'name'          => 'Newsroom sidebar (Top)',
            'id'            => 'top_newsroom_sidebar',
            'before_widget' => '<div>',
            'after_widget'  => '</div>',
        ));    

        register_sidebar(array(
            'name'          => 'Newsroom sidebar (Bottom)',
            'id'            => 'bottom_newsroom_sidebar',
            'before_widget' => '<div>',
            'after_widget'  => '</div>',
        ));    
    }

    /**
     * Register styles
     */
    public function register_styles()
    {}

    /**
     * Enqueue styles - used to enqueue global styles that are needed on all pages
     */
    public function enqueue_styles()
    {
        // Theme styles
        wp_enqueue_style(
            'main',
            THEME_URL . '/assets/dist/css/main.css',
            [],
            filemtime(THEME_DIR . '/assets/dist/css/main.css')
        );
    }

    /**
     * Deregister scripts
     */
    public function deregister_scripts()
    {
        //wp_deregister_script('jquery');
    }

    /**
     * Register scripts - used to register scripts that will later be enqueued on individual sections/pages/components
     */
    public function register_scripts() 
    {
        wp_register_script('employee-cta', THEME_URL . '/assets/dist/js/employee-cta.js', ['jquery'], filemtime(THEME_DIR . '/assets/dist/js/employee-cta.js'), true);
        wp_localize_script( 'employee-cta', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    }

    /**
     * Enqueue scripts - used to enqueue global scripts that are needed on all pages
     */
    public function enqueue_scripts()
    {
        // Theme scripts
        wp_enqueue_script(
            'main',
            THEME_URL . '/assets/dist/js/main.js',
            ['jquery'],
            filemtime(THEME_DIR . '/assets/dist/js/main.js'),
            true
        );

        wp_localize_script( 'main', 'ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    }

    public function enqueue_admin_styles() {
        wp_enqueue_style(
            'admin',
            THEME_URL . '/assets/dist/css/admin.css',
            [],
            filemtime(THEME_DIR . '/assets/dist/css/admin.css')
        );
        wp_enqueue_style(
            'admin-overrides',
            THEME_URL . '/assets/dist/css/admin-overrides.css',
            [],
            filemtime(THEME_DIR . '/assets/dist/css/admin-overrides.css')
        );
    }

    /**
     * Removes width and height from post thumbnails
     *
     * @hooked - post_thumbnail_html - 10
     *
     * @param string $html          The post thumbnail HTML.
     * @param int    $post_id       The post ID.
     * @param int    $post_image_id The post thumbnail ID.
     *
     * @return string Thumbnail HTML
     */
    public function remove_thumbnail_dimensions($html, $post_id, $post_image_id) {
        return preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    }


    public function reorder_admin_menu($__return_true) {
        return [
            'index.php', // Dashboard

            'separator1', // --Space--
            'edit.php?post_type=page', // Pages
            'edit.php', // Posts
            'edit-comments.php', // Comments
            'upload.php', // Media
            // 'edit.php?post_type=case_study',

            // 'separator2', // --Space--
            // 'themes.php', // Appearance
            // 'plugins.php', // Plugins
            // 'users.php', // Users
            // 'tools.php', // Tools
            // 'options-general.php', // Settings
        ];
    }

    /**
     * @param String $more - the default more text " [&hellip;]"
     */
    public function replace_excerpt_more($more)
    {
        global $post;
        return $more;
	    // return ' <a class="moretag" href="'. get_permalink($post->ID) . '">Read more...</a>';
    }

    /**
     * Override default WordPress search form
     *
     * @hooked get_search_form - 100 ($this->register_hooks())
     * @param string $form - Current HTML form
     * @return string - new form
     */
    public function get_search_form($form) {
        if (!$template = locate_template("templates/layout/search-form.php")) {
            return $form; // return current form if override not found
        }

        ob_start();
        include($template);
        return ob_get_clean();
    }

    /**
     * Set allowable upload mime types
     */
    function cc_mime_types($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    public function register_block_templates($args, $post_type) {
        switch ($post_type) {
            case 'page':
                $args['template'] = [
                    [
                        'acf/hero', []
                    ],
                ];
                break;
        }
        return $args;
    }

    public function allowed_block_types($blocks, $post) {
        return ['core/block', 'core/template', 'core/paragraph', 'core/image','core/list'];
    }

    public function update_posts_per_page( $query ) {
        if (!is_admin() && $query->is_main_query() && (is_home() || is_category())){
            $postsPerPage = isset($_GET['per-page']) && is_numeric($_GET['per-page']) ? $_GET['per-page'] : 5;
            if($postsPerPage > 20)
                $postsPerPage = 20;
            if($postsPerPage <= 5)
                $postsPerPage = 5;
            $query->set('posts_per_page', $postsPerPage);
        }
    }

    public function force_location() {
        $location = $_GET['location'];
        if ($location != null) {
            $_SERVER["HTTP_CF_IPCOUNTRY"] = $location;
        }
    }

    public static function get_current_region() {
        $location = $_SERVER["HTTP_CF_IPCOUNTRY"];
        return self::get_region($location);
    }

    public static function get_region($location) {
        if ($location == null) return null;

        $args = [
            'post_type' => 'region',
            'meta_query' => [
                [
                    'key' => 'country_codes',
                    'compare' => 'LIKE',
                    'value' => "{$location}"
                ]
            ]
        ];
        $query = new \WP_Query($args);

        // If we no regions were found and we aren't trying to get the global region, get the global region
        if ($query->found_posts == 0 && $location != 'global') {
            return self::get_region('global');
        }
        
        return $query->posts[0];
    }

    public static function get_all_regions($orderByLabel = false) {
        $args = array(
            'numberposts' => -1,
            'post_type' => 'region',
            'orderby'=> 'title', 
            'order' => 'ASC' 
        );
        if($orderByLabel){ 
            $args['orderby'] = 'meta_value';
            $args['meta_key'] = 'order';
        }
        return get_posts($args);
    }

    public static function get_region_page_url($globalPageId, $location = null)
    {
        $foundPage = null;

        if ($location != null) {
            $currentRegion = self::get_region($location);
        } else {
            $currentRegion = self::get_current_region();
        }
        
        if ($currentRegion != null) {
            $redirectPages = get_field('redirect_pages', $globalPageId);
            foreach ($redirectPages as $relationship) {
                if ($relationship['region']->ID == $currentRegion->ID) {
                    $foundPage = $relationship['page'];
                }
            }
        }

        return $foundPage;
    }
    

    public function spinner_url( $image_src, $form ) {
        return THEME_URL . '/assets/dist/images/loader.png';
    }

    public function set_gravityforms_country($value){
        $countries = get_country_codes_array();
        $location = $_SERVER["HTTP_CF_IPCOUNTRY"];
        return $countries[$location];
    }
    
    public function set_gravityforms_region($value){
        return self::get_current_region()->post_title;
    }

    public function wrap_gform_cdata_open( $content = '' ) {
        if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
            return $content;
        }
        $content = 'document.addEventListener( "DOMContentLoaded", function() { ';
        return $content;
    }

    public function wrap_gform_cdata_close( $content = '' ) {
        if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
            return $content;
        }
        $content = ' }, false );';
        return $content;
    }

    /**
     * Customize the Gravity Forms to CRM feed with Rich's specific data requirements
     */
    public function add_elogic_custom_data($case, $feed, $entry, $form, $form_id) {
        //populate the custom Contact field
        $case['elogic_contact'] = $case["CustomerId"];

        //truncate the product name to 100 to avoid error
        if(!empty($case['elogic_web_ProductName'])) {
            $case['elogic_web_ProductName'] = (strlen($case['elogic_web_ProductName']) > 99) ? substr($case['elogic_web_ProductName'],0,96).'...' : $case['elogic_web_ProductName'];
        }

        //lookup the product code (if applicable) and attach the entity ID
        if($crm = gf_dynamics_crm()) {

            if(!empty($case['elogic_Last5ofUPC'])) {
                $code_lookup = array();
                $code_lookup['ProductNumber'] = $case['elogic_Last5ofUPC'];

                $filter = rawurlencode( implode( ' and ', array_map( function( $item ) {
                    $key = $item[0];
                    $val = $item[1];
                    $val = str_replace( "'", "''", $val );
                    return "$key eq '$val'";
                }, array_map( null, array_keys( $code_lookup ), $code_lookup ) ) ) );

                $resource = 'ProductSet?$filter=' . $filter;
                
                $response = $crm->api->get($resource);

                if ( count( $response['results'] ) > 0 ) {
                    $product = $response['results'][0];
                    
                    $product_object = array();
                    $product_object['Id'] = $product['ProductId'];
                    $product_object['LogicalName'] = 'product';

                    $case['elogic_ProductId'] = $product_object;
                }

            }
        }

        return $case;
    }

    /**
     * Add hreflang sitemap file
     *
     * @return void
     */
    public function add_hreflang_sitemap() {
        // Get the upload directory
        $upload_dir = wp_upload_dir()['basedir'];
        // Check to see if the href_lang sitemap file exists
        $href_lang_file = "{$upload_dir}/hreflang_sitemap.xml";
        if (file_exists($href_lang_file)) {
            // Get the url to the href lang file
            $upload_url = wp_upload_dir()['baseurl'];
            $href_lang_url = "{$upload_url}/hreflang_sitemap.xml";
            // Get the modified time
            $time = date ("c", filemtime($href_lang_file));
            // Add this to the sitemap
            $sitemap = "<sitemap><loc>{$href_lang_url}</loc><lastmod>{$time}</lastmod></sitemap>";
        }

        return $sitemap;
    }
}
