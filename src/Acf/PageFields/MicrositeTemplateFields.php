<?php

namespace Cn\Acf\PageFields;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class MicrositeTemplateFields extends FieldGroup
{
    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('page_template', '==', 'page-template-microsite.php')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('microsite');
        $fields
            ->addTab('Main Navigation')
            ->addTrueFalse('hide_top_nav', ['label' => 'Hide Top Utility Navigation', 'instructions' => 'check to hide the utility nav.'])
            ->addTrueFalse('hide_main_nav', ['label' => 'Hide Main Navigation', 'instructions' => 'check to hide the main navigation and replace with different options.'])
            ->addRepeater('microsite_nav')->conditional('hide_main_nav', '==', 1)
                ->addLink('link')
            ->endRepeater()
            ->addTab('Footer')
            ->addTrueFalse('change_footer_bg', ['label' => 'Change Footer Background Image'])
            ->addImage('microsite_footer_bg')->conditional('change_footer_bg', '==', 1)
            ->addTrueFalse('hide_footer_nav', ['label' => 'Hide Footer Navigation', 'instructions' => 'check to hide the footer navigation.'])
            ->addRepeater('microsite_footer_nav')->conditional('hide_footer_nav', '==', 1)
                ->addLink('link')
            ->endRepeater()
            ->addTrueFalse('add_microsite_social', ['label' => 'Add Social Icons', 'instructions' => 'check to include social icons.'])->conditional('hide_footer_nav', '==', 1)
            ->addGroup('microsite_social')->conditional('add_microsite_social','==',1)
                ->addUrl('facebook')
                ->addUrl('twitter')
                ->addUrl('linkedin')
                ->addUrl('instagram')
                ->addUrl('youtube')
            ->endGroup();
        return $fields;
    }
}