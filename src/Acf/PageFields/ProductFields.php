<?php

namespace Cn\Acf\PageFields;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class ProductFields extends FieldGroup
{
    protected $title = "Region Settings";

    protected $position = 'side';

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $page = get_page_by_path('our-products');
        $this->setLocation('page_parent', '==', $page->ID)
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('product_category');
        $fields->addRelationship('regions', [
            'post_type' => 'region'
        ]);
        return $fields;
    }
}


