<?php

namespace Cn\Acf\PageFields;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class PageFields extends FieldGroup
{
    protected $position = 'side';

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('post_type', '==', 'page')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('pageFields');
        $fields->addTrueFalse('show_employee_cta', ['instructions' => 'Check this box to show the employee cta link in the bottom right corner of the page.']);
        return $fields;
    }
}


