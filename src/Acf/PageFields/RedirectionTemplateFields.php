<?php

namespace Cn\Acf\PageFields;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class RedirectionTemplateFields extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('page_template', '==', 'page-template-global.php')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('redirection');
        $fields
            ->addRepeater('redirect_pages')
                ->addFields($this->getRegionSelector())
                ->addPageLink('page', ['post_type'=> 'page', 'allow_archives' => 0])
                ->addTrueFalse('enable_autoredirect')
            ->endRepeater();
        return $fields;
    }
}