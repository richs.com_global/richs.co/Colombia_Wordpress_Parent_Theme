<?php

namespace Cn\Acf;

use StoutLogic\AcfBuilder\FieldsBuilder;

abstract class FieldGroup
{
    use ReusableFields;

    /**
     * @var StoutLogic\AcfBuilder\FieldsBuilder
     */
    protected $builder;

    protected $name;

    protected $title;

    protected $menu_order = 0;

    protected $position = 'normal';

    protected $style = 'default';

    protected $label_placement = 'top';

    protected $instruction_placement = 'field';

    protected $hide_on_screen = [];

    public function __construct($add_block_fields = true)
    {
        $this->builder = $this->makeBuilder($this->name(), $this->config());

        $this->build();
        
        if ($add_block_fields) {
            $this->addFields($this->getBlockPaddingSelect());
        }
    }

    abstract protected function build();

    public function getBuilder()
    {
        return $this->builder;
    }

    public function register()
    {
        add_action('acf/init', function() {
            acf_add_local_field_group( $this->builder->build() );
        });
    }

    public function makeBuilder($name, array $groupConfig = [])
    {
        return new FieldsBuilder($name, $groupConfig);
    }

    protected function name()
    {
        return snake_case($this->name ?? class_basename($this));
    }

    protected function title()
    {
        return $this->title ?? title_case(str_replace('_', ' ', snake_case(class_basename($this))));
    }

    public function config()
    {
        return [
            'title' => $this->title(),
            'menu_order' => $this->menu_order,
            'position' => $this->position,
            'style' => $this->style,
            'label_placement' => $this->label_placement,
            'instruction_placement' => $this->instruction_placement,
            'hide_on_screen' => $this->hide_on_screen
        ];
    }

    public function __call($method, $parameters)
    {
        return $this->builder->$method(...$parameters);
    }
}