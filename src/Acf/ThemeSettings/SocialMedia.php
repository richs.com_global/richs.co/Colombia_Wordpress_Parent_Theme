<?php

namespace Cn\Acf\ThemeSettings;

class SocialMedia extends \Cn\Acf\FieldGroup
{
    protected $menu_order = 150;

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('options_page', '==', 'theme-settings');
        $this->addUrl('facebook');
        $this->addUrl('twitter');
        $this->addUrl('linkedin');
        $this->addUrl('instagram');
        $this->addUrl('youtube');
        $this->addUrl('society6');
        $this->addUrl('dribble');
    }
}
