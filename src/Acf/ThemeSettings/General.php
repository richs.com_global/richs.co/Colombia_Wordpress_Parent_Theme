<?php

namespace Cn\Acf\ThemeSettings;

class General extends \Cn\Acf\FieldGroup
{
    protected $menu_order = 100;

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('options_page', '==', 'theme-settings');

        $this->addText('organization_name');

        $this->addTextarea('organization_address', [
                'rows' => '2',
                'new_lines' => 'br',
                'maxlength' => '100'
            ]);

        $this->addEmail('organization_email', ['label' => 'Email Address']);

        $this->addText('organization_phone', ['label' => 'Phone Number']);
    }
}
