<?php

namespace Cn\Acf\ThemeSettings;

class ScriptsStyles extends \Cn\Acf\FieldGroup
{
    protected $title = 'Scripts & Styles';

    protected $menu_order = 104;

    public function __construct()
    {
        parent::__construct(false);
    }

    protected function build()
    {
        $this->setLocation('options_page', '==', 'theme-settings');

        $this->addTab('Site Scripts');
        $this->addTextarea('head_scripts', [
            'new_lines' => '',
            'instructions' => "Scripts to be loaded in the page &lt;head /&gt;"
        ]);

        $this->addTextarea('after_body_open_scripts', [
            'new_lines' => '',
            'instructions' => 'Scripts to be loaded just after the opening &lt;body&gt; tag.'
        ]);

        $this->addTextarea('before_body_close_scripts', [
            'new_lines' => '',
            'instructions' => 'Scripts to be loaded just before the closing &lt;/body&gt; tag.'
        ]);

        $this->addTab('Marketo Settings');
        $this->addText('marketo_url');
        $this->addText('marketo_instanceId');
        $this->addText('marketo_thankyou_title',[
            'instructions' => 'A title you\'d like to appear after submitting a marketo form.'
        ]);
        $this->addText('marketo_thankyou_message',[
            'instructions' => 'A message you\'d like to appear after submitting a marketo form.'
        ]);
    }
}
