<?php

namespace Cn\Acf\CustomPostTypes;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Leaders extends FieldGroup
{

    protected $hide_on_screen = ['the_content'];

    protected function build()
    {
        $this->setLocation('post_type', '==', 'leader')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('leader');
        $fields
            ->addImage('headshot')
            ->addText('first_name')
            ->addText('last_name')
            ->addText('job_title')
            ->addWysiwyg('bio')
            ->addTrueFalse('display_featured_media')
            ->addRadio('media_type')
                ->conditional("display_featured_media", "==", '1')
                ->addChoice('video')
                ->addChoice('image')
            ->addGroup('featured_media_video')
                ->conditional("display_featured_media", "==", '1')
                ->conditional("media_type", "==", 'video')
                ->addImage("placeholder_image")
                ->addText("youtube_video_id")
            ->endGroup()
            ->addImage('featured_media_image')
                ->conditional("display_featured_media", "==", '1')
                ->conditional("media_type", "==", 'image')
            ->addWysiwyg('career_highlights')
            ->addWysiwyg('boards_and_leadership')
            ->addTextarea('favorite_meal')
            ->addTextarea('favorite_activity')
            ->addTextarea('best_part_job')
            ->addTextarea('worst_part_job')
            ->addRepeater('awards_and_honors')
                ->addText('text')
            ->endRepeater()
            ->addRepeater('books_published')
                ->addText('text')
            ->endRepeater();
            
        return $fields;
    }
}