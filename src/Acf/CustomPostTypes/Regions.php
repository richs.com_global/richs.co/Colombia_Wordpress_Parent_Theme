<?php

namespace Cn\Acf\CustomPostTypes;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Regions extends FieldGroup
{
    protected $hide_on_screen = ['the_content'];

    protected function build()
    {
        $this->setLocation('post_type', '==', 'region')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('region');
        $fields
            ->addTrueFalse("coming_soon", ['ui' => 1])->setInstructions("Displays 'Coming Soon' text on world map and location selector.")
            ->addFields($this->getCountryCodeSelect())
            ->addNumber("order")->setWidth("50%")
            ->addSelect("group_label", ['width' => '50%'])->setInstructions("The grouping for the marketing sites listed under 'See All Destinations At a Glance'")
                ->addChoice('Global')
                ->addChoice('Asia')
                ->addChoice('Africa')
                ->addChoice('North America')
                ->addChoice('Latin America')
                ->addChoice('Middle East')
                ->addChoice('Europe')
                ->addChoice('Australia')
            ->addTextArea('country_codes')->setInstructions('A comma separated list of country codes for this region.')
                ->addAccordion('marketing_sites_group', ['label' => 'Marketing Sites'])
                    ->addRepeater('marketing_sites', ['label' => 'Sites'])
                        ->addLink('link')
                    ->endRepeater()
                ->addAccordion("map_settings")
                    ->addText("x_coordinate", ['instructions' => 'If initial global map popup, this is the "left" position of the popup in percent'])->setWidth("50%")
                    ->addText("y_coordinate", ['instructions' => 'If initial global map popup, this is the "top" position of the popup in percent'])->setWidth("50%");
        return $fields;
    }
}