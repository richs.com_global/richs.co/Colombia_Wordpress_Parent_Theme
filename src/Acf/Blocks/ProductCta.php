<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Cn\Acf\ReusableFields;

class ProductCta extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/product-cta')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('product-cta');
        $backgroundField = $this->getBackgroundColorSelect();
        $fields
            ->addText('title')
            ->addImage('category_icon')
            ->addWysiwyg('text')
            ->addImage('background_image');
        return $fields;
    }
}