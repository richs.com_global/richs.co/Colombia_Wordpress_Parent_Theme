<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class ImageGrid extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/image-grid')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('image-grid');
        $fields
            ->addText('title')
            ->addImage('image_tall_vertical', ['label' => "Tall image"])
                ->setInstructions("Add a tall vertical image 269x568")
            ->addImage('image_small_1', ['label' => "Small Image 1"])
                ->setInstructions("Add a small image 269x269")
            ->addImage('image_small_2', ['label' => "Small Image 2"])
                ->setInstructions("Add a small image 269x269")
            ->addImage('image_large', ['label' => "Large Image"])
                ->setInstructions("Add a large image 568x568");
        return $fields;
    }
}