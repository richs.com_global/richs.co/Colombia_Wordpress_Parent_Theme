<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class GravityForm extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/gravity-form')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('gravity-form');
        $fields
            ->addTab('content_settings')
            ->addSelect('style', ['default_value' => "one-column", 'choices' =>
            [
                ['one-column' => "One Column"],
                ['two-columns' => "Two Columns"],
            ]])
            ->addText('title')
            ->addTextarea('text')
            ->addNumber('gravity_form_id')
            ->addTrueFalse('redirect_after_submission')
            ->addUrl('redirect_url')
                ->conditional('redirect_after_submission', '==', 1)
            ->addFields($this->getBackgroundColorSelect())
            ->addTab('html_options')
                ->addFields($this->getHtmlAttributesField());

            
        return $fields;
    }
}