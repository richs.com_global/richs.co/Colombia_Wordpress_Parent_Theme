<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Wysiwyg extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/wysiwyg')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('wysiwyg');
        $fields
            ->addText('title')
            ->addWysiwyg('content')
            ->addFields($this->getBackgroundColorSelect());
        return $fields;
    }
}