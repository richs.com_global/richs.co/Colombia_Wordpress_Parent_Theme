<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class SearchBar extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/search-bar')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('search_bar');
        $fields->addText('title');
        return $fields;
    }
}