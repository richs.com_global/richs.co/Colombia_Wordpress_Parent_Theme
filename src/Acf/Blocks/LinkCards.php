<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class LinkCards extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/link-cards')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('link-cards');
        $fields
            ->addText('title')
            ->addRepeater('links')
                ->addText('link_title')
                ->addTextarea('link_text')
                ->addLink('link')
            ->endRepeater();
        return $fields;
    }
}