<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class TextWithLinks extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/text-with-links')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('text-with-links');
        $fields->addText('title')
            ->addWysiwyg('text')
            ->addLink('button')
            ->addRepeater('links')
                ->addLink('link')
            ->endRepeater();
            //->addFields($this->getBackgroundColorSelect());
        return $fields;
    }
}