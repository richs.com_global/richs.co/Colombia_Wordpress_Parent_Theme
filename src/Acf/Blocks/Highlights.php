<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Highlights extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/highlights')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('highlights');
        $fields
            ->addText('title')
            ->addFields($this->getBackgroundColorSelect())
            ->addRepeater('items')
                ->addGroup('header', ['label' => "Header"])
                    ->addSelect('type', ['choices' => [
                        ['text' => "Text"],
                        ['image' => "Image"]
                    ]])
                    ->addText('text')
                        ->conditional('type', '==', 'text')
                    ->addImage('image')
                        ->conditional('type', '==', 'image')
                ->endGroup()
                ->addTextArea('label')
                ->addTextArea('text')
            ->endRepeater();
        return $fields;
    }
}