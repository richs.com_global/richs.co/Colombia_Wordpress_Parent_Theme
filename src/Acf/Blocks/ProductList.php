<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class ProductList extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/product-list')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('product-list');
        
        $globalPageSelector = $this->getGlobalPageSelector();
        $globalPageSelector->getField('global_page')->conditional('use_redirection_button', '==', 1);

        $fields
            ->addText('title')
            ->addImage('background_image')
            ->addTrueFalse('use_redirection_button', ['instructions' => "Check this box if you want this button to redirect to a certain page based on the user's region"])
            ->addLink('button', ['label' => "Call To Action Button"])
                ->conditional('use_redirection_button', '==', 0)
            ->addFields($globalPageSelector)
            ->addText('button_text')
                ->conditional('use_redirection_button', '==', 1)
            ->addPostObject('fallback_page')
                ->conditional('use_redirection_button', '==', 1)
            ->addRepeater('products')
                ->addImage('image', ['label' => "Product Image"])
                ->addText('title')
                ->addTextArea('text')
            ->endRepeater();
        return $fields;
    }
}