<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Map extends FieldGroup
{

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/map')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('link-cards');
        $fields
            ->addText('title')
            ->addTextArea('sub-title')
            ->addImage("background_image");
        return $fields;
    }
}