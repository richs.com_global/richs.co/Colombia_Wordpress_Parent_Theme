<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class TabbedForm extends FieldGroup
{

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/tabbed-form')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('tabbed_form');
        $fields
            ->addText("title")
            ->addRepeater('tabs', ['layout' => 'block'])
                ->addText('title')
                ->addNumber('gravity_form_id');
        return $fields;
    }
}