<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Cn\Acf\ReusableFields;

class Pillars extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/pillars')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('pillars');
        $fields
            ->addRepeater('pillars')
                ->addImage('image')
                ->addText('title')
                ->addWysiwyg('content')
            ->endRepeater();
        return $fields;
    }
}