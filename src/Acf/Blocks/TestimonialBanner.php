<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;

class TestimonialBanner extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/testimonial-banner')
            ->addRepeater('slides')
                ->addText('title')
                ->addText('name')
                ->addFields($this->getCountryCodeSelect())
                ->addText('occupation')
                ->addWysiwyg('content')
                ->addTrueFalse("swap_name_content", ['ui' => 1, 'label' => 'Swap Name & Content'])->setInstructions("Moves the content above the name and makes the text larger.")
                ->addImage('background_image')
                ->addFields($this->getBackgroundOverlay())
                ->addLink('button')
            ->endRepeater();
    }
}