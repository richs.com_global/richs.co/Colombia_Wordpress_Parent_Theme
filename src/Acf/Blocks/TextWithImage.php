<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class TextWithImage extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/text-with-image')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('text-with-image');
        $fields
            ->addImage('image')
            ->addSelect('image_position', ['default_value' => "left", 'choices' =>
            [
                ['left' => 'Left'],
                ['right' => 'Right']
            ]])
            ->addSelect('image_size', ['default_value' => "default", 'choices' =>
            [
                ['default' => "Default"],
                ['medium' => "Medium"],
                ['small' => "Small"]
            ]])
            ->addText('title')
            ->addWysiwyg('text')
            ->addLink('button');
        return $fields;
    }
}