<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;

class SeeAllDestinations extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/see-all-destinations')
            ->addImage('background_image')
            ->addText("title")
            ->addLink("button_one")->setWidth("50%")
            ->addLink("button_two")->setWidth("50%")
            ->addNumber('gravity_form_id');
    }
}