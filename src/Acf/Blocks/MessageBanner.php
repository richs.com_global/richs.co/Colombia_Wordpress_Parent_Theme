<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;

class MessageBanner extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/message-banner')
            ->addRepeater('slides')
                ->addText('title')
                ->addFields($this->getFontSelector())
                ->addWysiwyg('content')
                ->addImage('background_image')
                ->addFields($this->getBackgroundOverlay())
                ->addLink('button')
            ->endRepeater();
    }
}