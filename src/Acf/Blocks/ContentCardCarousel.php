<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class ContentCardCarousel extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/content-card-carousel')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('content-card-carousel');
        $fields->addText('title')
            ->addFields($this->getCardSizeSelect())
            ->addFields($this->getCountryCodeSelect())
            ->addRepeater('cards', [
                'layout' => 'block',
            ])
                ->addGroup('card_header')                    
                    ->addText('text')
                ->endGroup()
                ->addText('card_title')
                ->addTextarea('card_copy')
                ->addFields($this->getCardButtons())
                ->addLink('card_background_link')
                ->addTrueFalse('use_background_video', ['label' => "Use Background Video"])
                ->addImage('background_image')
                ->addFile("background_video", ['mime_types' => 'mp4'])->conditional('use_background_video', '==', 1)
                ->addFields($this->getBackgroundOverlay())
                ->addTrueFalse('disable_hover', ['label' => "Disable Hover Effect?"])
            ->endRepeater();
        return $fields;
    }
}