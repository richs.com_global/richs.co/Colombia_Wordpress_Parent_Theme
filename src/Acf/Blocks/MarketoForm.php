<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Cn\Acf\ReusableFields;

class MarketoForm extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/marketo-form')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('marketo_form');
        $fields
            ->addSelect('style', ['default_value' => "two-columns", 'choices' =>
            [
                ['two-columns' => "Two Columns"],
            ]])
            ->addText('title')
            ->addTextArea('text', ['new_lines' => 'br'])
            ->addText('marketo_id', ['default_value' => "1044", 'label' => "Marketo Form ID"])
            ->addFields($this->getBackgroundColorSelect());
        return $fields;
    }
}