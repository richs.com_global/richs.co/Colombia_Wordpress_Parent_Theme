<?php

namespace Cn\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class TextWithVideo extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/text-with-video')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('text-with-video');
        $fields
            ->addText('title')
            ->addRepeater('slides')
                ->addText('title')
                ->addWysiwyg('text')
                ->addLink('button')
                ->addTrueFalse('use_image', ['label' => "Use Image"])
                ->addImage('image')->conditional('use_image', '==', '1')
                ->addFile('video', ['mime_types' => 'mp4'])->conditional('use_image', '==', '0')
                ->addSelect('video_position', ['default_value' => "left", 'choices' =>
                [
                    ['left' => 'Left'],
                    ['right' => 'Right']
                ]])
            ->endRepeater();
            
        return $fields;
    }
}