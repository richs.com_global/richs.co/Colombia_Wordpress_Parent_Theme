<?php
use Cn\Theme;

/**
 * Template Name: Microsite Page
 *
 * This page handles the microsite page layout and customizations
**/

get_header(); 

loop_posts(function() {
    render('page-content');
});

get_footer();