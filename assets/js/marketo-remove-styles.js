if(typeof MktoForms2 != "undefined") {
    MktoForms2.whenReady( function(_form) {
        var form = $('.mktoForm');
        if (form) {
            $('#mktoForms2BaseStyle').remove(); 
            $('#mktoForms2ThemeStyle').remove();
            form.find('.mktoButtonWrap').css('margin-left', 0);
            form.find('.mktoAsterix').remove();
            form.find('.mktoButton').removeClass('mktoButton');
            // remove inline widths set by marketo
            form.css('width', '');
            form.find('input, label, select, textarea, .mktoRadioList').css('width', '');
            form.find('select').wrap('<div class="select-wrapper"></div>');
        }
    });
}