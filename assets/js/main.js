/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries.
 */
 
require('./bootstrap');
require('./countrycode');
require('./regional-links');
require('./marketo-remove-styles');
require('./print-button');
require('./mobile-menu');
require('./form-country-region');
require('./components/banner-component');
require('./components/video-component');
require('./components/text-with-video-component');
require('./components/content-card-carousel-component');
require('./components/testimonial-component');
require('./components/scroll-to-explore');
require('./components/gravity-form-component');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page via #theme. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const theme = new Vue({
    el: '#theme',

    data() {
        return {
            //
        }
    },

    /**
     * The component has been created by Vue.
     */
    created() {
        var self = this;
    },

    mounted() {
        this.whenReady();
    },

    methods: {
        /**
         * Finish bootstrapping the application.
         */
        whenReady() {
            //
        },
    },
});