require('remodal');

$(function () {
    // Hide modal on outside click
    $('.remodal.video').on('click', function() {
        $(this).remodal().close();
    });

    $(document).on('opening', '.remodal.video', function (e) {
        var $modal = $(this);
        var $video = $modal.find('.video');
        $video[0].src = $video.attr('data-video');
    });
    
    $(document).on('closing', '.remodal.video', function (e) {
            var $modal = $(this);
            var $video = $modal.find('.video');
            var $src = $video[0].src;

            $video[0].src = "";
        });
});