import {TimedBanner} from './timed-banner';

$(function () {
    // Get all timed carousels
    $('.banner .timed-carousel').each(function() {
        new TimedBanner($(this));
    });
});