$(function () {
    $('.content-card-carousel').each(function() {
        var $carousel = $(this);
        
        initVideos($carousel);
    
        
        var breakpoint = function(event, slick, currentSlide) {
            playVideo(slick.$slides);
        };

        $carousel.on('breakpoint', breakpoint);
    });

    function initVideos($carousel) {
        var $slides = $carousel.find('.slick-slide');
        playVideo($slides);
    }

    function playVideo($slides){
        $slides.each(function(){
            var video = $(this).find('video');
            if(video.length > 0)
                video[0].play();
        });
    }

});