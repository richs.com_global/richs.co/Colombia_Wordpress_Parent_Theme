const intervalDelay = 50;

export class CarouselProgressBar {
    constructor(element, options, callback) {
        this.element = element;
        this.interval = 0;
        this.isPaused = false;

        if (options != null) {
            this.time = options['time'];
        }

        this.progressIndicator = this.element.find('.progress');
        if (this.progressIndicator.length == 0) {
            this.progressIndicator = $('<div class="progress"></div>');
            element.append(this.progressIndicator);
        }

        this.callback = callback;

        var self = this;
        var slide = element.closest('.slick-slide');
        slide.mouseover(function (){
            self.isPaused = true;
        });
        slide.mouseout(function() {
            self.isPaused = false;
        });
    }

    setTotalTime(time) {
        this.time = time;
    } 

    start() {
        this.timerInterval = setInterval(this.increment.bind(this), intervalDelay);
    }

    increment() {
        if (!this.isPaused) {
            this.interval += intervalDelay;
            var percentage = this.interval / this.time;

            this.progressIndicator.animate({
                width: percentage * 100 + "%"
            }, 0);

            if (percentage >= 1) {
                clearInterval(this.timerInterval);
                setTimeout(this.finish.bind(this), intervalDelay);
            }
        }
    }
    
    finish() {
        this.callback();
        setTimeout(this.reset.bind(this), 500);
    }

    stop(isReset) {
        clearInterval(this.timerInterval);
        if (isReset === true) {
            this.reset();
        }
    }

    reset() {
        this.progressIndicator.width(0);
        this.interval = 0;
    }
}

