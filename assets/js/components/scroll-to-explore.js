$(function () {
    $('.hero .scroll-to-explore').on('click', function(e) {
        $('html, body').stop().animate({
            scrollTop: $(this).closest('section').next().offset().top - 121
        }, 500);
        e.preventDefault();
    });
});