$(function(){
    window.addEventListener('rpCountryInitalized', function() {
        var countrySelect = $('form .gfield.country select');
        countrySelect.val(window.countryName);
        countrySelect.trigger('change');

        var regionSelect = $('form .gfield.region-hidden input');
        regionSelect.val(window.region)
    });
});