window._ = require('lodash');
window.moment = require('moment');
window.axios = require('axios');

//for < Edge maps
if (!Object.entries) {
    Object.entries = function( obj ){
        var ownProps = Object.keys( obj ),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array
        while (i--)
        resArray[i] = [ownProps[i], obj[ownProps[i]]];
        
        return resArray;
    };
}

/*
 * Load jQuery and Bootstrap jQuery, used for front-end interaction.
 */
if (window.$ === undefined) {
    window.$ = window.jQuery;
}

window.Vue = require('vue');

require('./vue/directives');
require('./vue/components');