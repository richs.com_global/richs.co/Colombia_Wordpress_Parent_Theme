$(function() {

    $.ajax({
        url: "https://www.richs.com/cf-visitor",
        type: "GET",
        success: function(response) {
            get_visitor_params(JSON.parse(response));
        }
    })

    function get_visitor_params(cfResponse) {
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'POST',
            data: {'action': 'get_visitor_params', 'countryCode': cfResponse.countryCode, 'workLocation': cfResponse.workLocation},
            success: function(response){
                window.region = response.region;
                window.countryCode = response.countryCode;
                window.countryName = response.countryName;
                window.marketingSites = response.marketingSites;
                if (response.employeeCta != null) {
                    window.employeeCta = response.employeeCta;
                }

                var event; // The custom event that will be created
                if(document.createEvent){
                    event = document.createEvent("HTMLEvents");
                    event.initEvent("rpCountryInitalized", true, true);
                    event.eventName = "rp_CountryInitalized";
                    window.dispatchEvent(event);
                } else {
                    event = document.createEventObject();
                    event.eventName = "rpCountryInitalized";
                    event.eventType = "rpCountryInitalized";
                    window.fireEvent(event.eventType, event);
                }
            }
        });
    }
})