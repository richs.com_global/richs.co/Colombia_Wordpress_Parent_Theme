/** Directive */
Vue.directive('click-outside', require('./directives/ClickOutside.vue').default);
/*Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      // here I check that click was outside the el and his childrens
      if (!(el == event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
});*/

/**
 * Components
 */
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('on-click-outside', require('./components/OnClickOutside.vue').default);
Vue.component('slick-carousel', require('./components/SlickCarousel.vue').default);
Vue.component('main-nav', require('./components/MainNavigation.vue').default);
Vue.component('marketo-form', require('./components/MarketoForm.vue').default);
Vue.component('tabs', require('./components/Tabs.vue').default);
Vue.component('tab', require('./components/Tab.vue').default);
Vue.component('destinations', require('./components/Destinations/Destinations.vue').default);
Vue.component('content-box', require('./components/Destinations/ContentBox.vue').default);
Vue.component('world-map', require('./components/WorldMap.vue').default);
Vue.component('posts-per-page', require('./components/PostsPerPage.vue').default);
Vue.component('text-with-map', require('./components/TextWithMap.vue').default);
Vue.component('region-selector', require('./components/RegionSelector.vue').default);
Vue.component('region-page-dropdown', require('./components/RegionPageDropdown.vue').default);
Vue.component('timeline', require('./components/Timeline.vue').default);

