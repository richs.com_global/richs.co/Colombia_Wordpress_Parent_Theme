# Richs.com WordPress Parent Theme
The Richs.com WordPress development will use the following technology stack and tooling. This tech stack has been determined based on Cypress North’s experience in WordPress, PHP, and software engineering along with consideration for long term viability and support of packages. While it’s not required to use the same tech stack and tooling for future WordPress development, there are advantages to having a common groundwork. 

# Web Stack
- PHP
- HTML5
- [Sass](https://sass-lang.com/)
- [jQuery](https://jquery.com/)
- [Webpack](https://webpack.js.org/) / Mix

# Required Developer Packages
- PHP 7.1.3+
- [Composer](https://getcomposer.org/)
- [NodeJs](https://nodejs.org/en/) / NPM
- [Git](https://git-scm.com/)

# Included but Optional frameworks
- [VueJs](https://vuejs.org/)
- [TailwindCss](https://tailwindcss.com)

# Development Tools
- [Visual Studio Code](https://code.visualstudio.com/)

# Local Web server and Database
Web Server: Nginx
Database: MariaDB / MySql

[Laragon](https://laragon.org/) is an easy way to install and configure the local web server and database

# Required WordPress Plugins
- [Advanced Custom Fields Pro](https://www.advancedcustomfields.com/pro/)
  * License Key: ```b3JkZXJfaWQ9NzI3NzB8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTAxLTE1IDIyOjE2OjE3```

# Recommended WordPress Plugins
- Gravity Forms
- Gravity Forms Gutenberg Add-On
- Yoast SEO
- Disable Author Archives
- Sucuri Security
- HTTP Auth

# Developer Installation
From the root of the theme folder, run the following commands:

```composer install```

```npm install```

```npm run dev```

You can configure webpack.mix.js to set browserSync to your local webserver URL. Once configured, you can run ```npm run watch``` to launch a syncronized browser and have the project auto-compile the sass and js every time you save. 

# Child Theme Development
In order to receive updates to this theme, all new development should be done in a Child WordPress Theme. The Child Theme can inherit from this theme and then extend and/or override as needed.


# Deployment to Production
When deploying a project, there are two preparation steps.
1. Compile the site for production by running ```npm run production```
2. Include the composer packages if you do not have SSH access to the hosting
  * include the ```vendors``` folder (remove from .gitignore) and then run ```composer install --no-dev```